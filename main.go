package main

import (
	"log"
	"os"
	"sync"
	//"path/filepath"
	"github.com/kkdai/youtube"
	"flag"
	"strings"
)

var wg = &sync.WaitGroup{}

var (
	searchterm    = flag.String("searchterm", "", "Name of video file to upload")
)
func main() {
	if _, err := os.Stat("./vid_files/"); os.IsNotExist(err) {
		os.Mkdir("./vid_files/", 0755)
	}

	/* url1 := "https://www.youtube.com/watch?v=EBPAoZC2nM8"
	url2 := "https://www.youtube.com/watch?v=V8c9qClsdnM"
	wg.Add(1)
	go downloadYouTubeVideo(url1)

	wg.Add(1)
	go downloadYouTubeVideo(url2)

	wg.Wait() */

	getVideoInfo()

	// filename, title, (replace pipes with spaces, then replace spaces with commas)
	// loop through database, if uploaded == 1, go to next entry until uploaded == 0, after upload set uploaded = 1
	// delete file from local filesystem to save space
	uploadVideo()
}

func downloadYouTubeVideo(url string) {
	defer wg.Done()
	y := youtube.NewYoutube(true)

	y.DecodeURL(url)
	if len(y.StreamList) < 1 {
		log.Fatalln("ERROR: Stream list is 0")
	}
	currentFile := strings.Replace(y.StreamList[0]["title"], " ", "_", -1)
	log.Println("FILENAME: ", currentFile)

	for _, stream := range y.StreamList {

		log.Println(stream["title"])
		log.Println(stream["type"])
	}

	y.StartDownload("./vid_files/" + currentFile)

}
