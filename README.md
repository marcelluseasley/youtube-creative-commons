**YouTube Creative Commons Downloader/Uploader**
---

This is an application that will take a search term and return a list of Creative Common-type YouTube videos and will download them, modify the title, then upload them to my YouTube account. Since the licenses are Creative Common, they are copyright-free. With Adsense enabled, hopefully monetization will kick in. ;-)

---
## Download Videos

Uses the Youtube API, by submitting a search term (i.e. `fitness`) and returning a list of Creative Common video IDs, along with the video titles.

The application then downloads thumbnail pictures from unsplash.com via the Unsplash API, using the same search term from the video ID and title download.

Using the video IDs, the application proceeds to download the video, then re-upload to my YouTube with a modified title and random related thumbnail from unsplash.com.


## Account Info

* Email Address: h31s3nb0rg@gmail.com
* M....2
